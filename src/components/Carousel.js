import React, { Component } from 'react';
import img1 from '../assets/images/carousel-2.jpg';
import img2 from '../assets/images/carousel-3.jpg';
import img3 from '../assets/images/carousel-4.jpg';
import img4 from '../assets/images/carousel-5.jpg';
import img5 from '../assets/images/carousel-6.jpg';
import img6 from '../assets/images/carousel-7.jpg';
import img7 from '../assets/images/carousel-8.jpg';
import img8 from '../assets/images/carousel-9.jpg';
import img9 from '../assets/images/carousel-10.jpg';
import img10 from '../assets/images/carousel-11.jpg';
import img11 from '../assets/images/carousel-12.jpeg';

class Carousel extends Component {
    render() {
        return (
            <div className='carousel'>
                <div class="container py-3">
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel" data-interval="1000">
                        <div class="carousel-inner">
                            <div class="carousel-item active text-center">
                                <img src={img1} alt="img1" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img2} alt="img2" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img3} alt="img3" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img4} alt="img4" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img5} alt="img5" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img6} alt="img6" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img7} alt="img7" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img8} alt="img8" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img9} alt="img9" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img10} alt="img10" class="carousel-img img-fluid" />
                            </div>
                            <div class="carousel-item text-center">
                                <img src={img11} alt="img11" class="carousel-img img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>
                <br /><br />
            </div>
        );
    }
}

export default Carousel;