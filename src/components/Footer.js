import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer class="ms-5">
                <h6>&copy;2023 Instituto Tecnológico de Informática</h6>
                <h6>***********************************************</h6>
                <h6>Montevideo, Uruguay</h6>
                <h6>Teléfono: ******** - Fax: ******** Interno **</h6>
                <h6>Diseño de los alumnos: *********** - ************ del grupo ** **</h6>
            </footer>
        );
    }
}

export default Footer;