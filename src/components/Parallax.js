import React, { Component } from 'react';

class Parallax extends Component {
    render() {
        return (
            <div className='parallax'>
                <div class="container py-5">
                    <div class="col-sm-6 col-md-8 col-lg-10 banner">
                        <div class="container py-5">
                            <div class="row text-center" id="opciones-parallax">
                                <div class="col-sm-6 col-md-3 col-lg-3">
                                    <button type="button" class="btn btn-link" data-bs-toggle="modal"
                                        data-bs-target="#staticBackdrop">Horarios</button>
                                    {/* MODAL */}
                                    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false"
                                        tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Seleccionar turno:</h1>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <ul>
                                                        <li>
                                                            <a href="#">matutino</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">vespertino</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">nocturno</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3 col-lg-3">
                                    <button type="button" class="btn btn-link">Novedades</button>
                                </div>
                                <div class="col-sm-6 col-md-3 col-lg-3">
                                    <button type="button" class="btn btn-link">Inscripciones</button>
                                </div>
                                <div class="col-sm-6 col-md-3 col-lg-3">
                                    <button type="button" class="btn btn-link">Calendario de exámenes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Parallax;