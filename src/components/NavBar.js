import React, { Component } from 'react';
import logo from '../assets/images/Logo_opcion_1_original.png';

class NavBar extends Component {
    render() {
        return (
            <div className='nav-bar'>
                <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
                    <a class="navbar-brand ms-4" href="index.html"> <img src={logo} class="img-fluid"
                        width="50px" height="50px" alt="logo" title="Página principal I.T.I." /></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarITI"
                        aria-controls="navbarITI" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarITI">
                        <div id="opciones-navbar">
                            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                                <li class="nav-item mx-4">
                                    <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Inicio
                                    </a>
                                </li>
                                <li class="nav-item dropdown mx-4">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        Estudiantes
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#">
                                            Programas
                                        </a>
                                        </li>
                                        <li>
                                            <hr class="dropdown-divider" />
                                        </li>
                                        <li><a class="dropdown-item" href="#">Horarios</a></li>
                                        <li>
                                            <hr class="dropdown-divider" />
                                        </li>
                                        <li><a class="dropdown-item" href="#">Exámenes</a></li>
                                        <li>
                                            <hr class="dropdown-divider" />
                                        </li>
                                        <li><a class="dropdown-item" href="#">Aviso de inasistencias</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown mx-4">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        Docentes
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="#">Reuniones</a></li>
                                        <li>
                                            <hr class="dropdown-divider" />
                                        </li>
                                        <li><a class="dropdown-item" href="#">Avisos</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item mx-4">
                                    <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Biblioteca
                                    </a>
                                </li>
                                <li class="nav-item mx-4">
                                    <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Novedades
                                    </a>
                                </li>
                                <li class="nav-item mx-4">
                                    <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Contacto
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <br /><br /><br /><br />
            </div>
        );
    }
}

export default NavBar;