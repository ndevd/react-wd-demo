//import './App.scss';
import NavBar from './components/NavBar';
import Carousel from './components/Carousel';
import Parallax from './components/Parallax';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <NavBar />
      <div class="alert alert-dark" role="alert">
        La siguiente es una demo del proyecto de la materia Diseño Web, realizada con <span id='react'><strong>React</strong></span>.
      </div>
      <div class="container py-3 text-center">
        <h1 class="display-6 mt-4"><b>Instituto Tecnológico de Informática</b></h1>
      </div>
      <Carousel />
      <Parallax />
      <Footer />
      {/* https://gitlab.com/ndevd */}
      <p id='nde' class="text-center">neodev &copy;</p>
    </div>
  );
}

export default App;
